
#include <fstream>
using namespace std;

#include "detection.h"
using namespace Etiseo;

void main()
{
	Etiseo::UtilXml::Init();

	Detection obj_detection;

	ifstream is;
	is.open("detection.xml");
	obj_detection.fromXml(is);
	is.close();

	vector<vector<DETECTION_OBJECT>> detection_result = obj_detection.get_detection_result();

	Etiseo::UtilXml::Cleanup();

	getchar();
}