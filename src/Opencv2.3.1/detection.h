#ifndef _DETECTION_XML_H_
#define _DETECTION_XML_H_

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
using namespace cv;

#include "xmlUtil.h"

#include <iostream>
#include <vector>
using namespace std;


#define XML_TAG_VIDEO			BAD_CAST"Video"
#define XML_TAG_FNAME			BAD_CAST"fname"
#define XML_TAG_START_FRAME		BAD_CAST"start_frame"
#define XML_TAG_END_FRAME		BAD_CAST"end_frame"

#define XML_TAG_FRAME			BAD_CAST"Frame"
#define XML_TAG_NO				BAD_CAST"no"

#define XML_TAG_OBJECTLIST		BAD_CAST"ObjectList"
#define XML_TAG_STATUS			BAD_CAST"status" 

#define XML_TAG_OBJECT			BAD_CAST"Object"
#define XML_TAG_OBJ_ID			BAD_CAST"obj_id" 
#define XML_TAG_OBJ_TYPE		BAD_CAST"obj_type"
#define XML_TAG_CONFIDENT		BAD_CAST"confident" 
#define XML_TAG_SCORE			BAD_CAST"score" 

#define XML_TAG_RECT			BAD_CAST"Rect"
#define XML_TAG_X				BAD_CAST"x"
#define XML_TAG_Y				BAD_CAST"y"
#define XML_TAG_WIDTH			BAD_CAST"width"
#define XML_TAG_HEIGHT			BAD_CAST"height"

#define XML_TAG_VISIBILITY		BAD_CAST"Visibility"
#define XML_TAG_FULL			BAD_CAST"full"
#define XML_TAG_UPPER			BAD_CAST"upper"
#define XML_TAG_LOWER			BAD_CAST"lower"
#define XML_TAG_LEFT			BAD_CAST"left"
#define XML_TAG_RIGHT			BAD_CAST"right"


struct DETECTION_OBJECT
{
	int id;
	Rect rect;
	float visibility;
};


namespace Etiseo 
{
	//!  A root class handling detection results
	class Detection
	{
	public:
		//! Constructor
		Detection();
		//! Destructor
		virtual ~Detection();

		//! Access to members
		vector<vector<DETECTION_OBJECT>> get_detection_result(){return detection_result;}

		//! Loading from an XML
		virtual void fromXml(istream& is);
		//! Saving to an XML
		virtual void toXml(ostream& os) const;


		//////////////////////////////////////////////////////////////////////////
		protected:


		//////////////////////////////////////////////////////////////////////////
		private:
		vector<vector<DETECTION_OBJECT>> detection_result;
	};
};

#endif
