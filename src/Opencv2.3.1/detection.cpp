#include <libxml/xmlwriter.h>
#include <libxml/xmlreader.h>

#include "detection.h"
using namespace Etiseo;

Detection::Detection()
{
}

Detection::~Detection()
{
}

void Detection::fromXml(istream& is)
{
	xmlDocPtr doc = xmlReadIO(UtilXml::ReadCallback, UtilXml::InputCloseCallback, &is,  NULL, NULL, 0);
	xmlNodePtr node = xmlDocGetRootElement(doc);

	if (node) 
	{
		char *temp;
		if (xmlStrcmp(node->name, XML_TAG_VIDEO) == 0) 
		{
			xmlNodePtr child = node->xmlChildrenNode;
			while (child != NULL) 
			{
				if (xmlStrcmp(child->name, XML_TAG_FRAME) == 0) 
				{
					vector<DETECTION_OBJECT> stub_image;

					xmlNodePtr child_L2 = child->xmlChildrenNode;
					if (xmlStrcmp(child_L2->name, XML_TAG_OBJECTLIST) == 0)
					{
						xmlNodePtr child_L3 = child_L2->xmlChildrenNode;
						while (child_L3 != NULL)
						{
							if (xmlStrcmp(child_L3->name, XML_TAG_OBJECT) == 0)
							{
								DETECTION_OBJECT stub;

								temp = (char*)xmlGetProp(child_L3, XML_TAG_OBJ_ID);
								if (temp) 
								{
									stub.id = atoi(temp);
									xmlFree(temp); 
								}

								xmlNodePtr child_L4 = child_L3->xmlChildrenNode;
								while (child_L4 != NULL)
								{
									if (xmlStrcmp(child_L4->name, XML_TAG_RECT) == 0)
									{
										temp = (char*)xmlGetProp(child_L4, XML_TAG_X);
										if (temp) 
										{
											stub.rect.x = (int)(atof(temp)+0.5);
											xmlFree(temp); 
										}
										temp = (char*)xmlGetProp(child_L4, XML_TAG_Y);
										if (temp) 
										{
											stub.rect.y = (int)(atof(temp)+0.5);
											xmlFree(temp); 
										}
										temp = (char*)xmlGetProp(child_L4, XML_TAG_WIDTH);
										if (temp) 
										{
											stub.rect.width = (int)(atof(temp)+0.5);
											xmlFree(temp); 
										}
										temp = (char*)xmlGetProp(child_L4, XML_TAG_HEIGHT);
										if (temp) 
										{
											stub.rect.height = (int)(atof(temp)+0.5);
											xmlFree(temp); 
										}
									}
									else if (xmlStrcmp(child_L4->name, XML_TAG_VISIBILITY) == 0)
									{
										temp = (char*)xmlGetProp(child_L4, XML_TAG_FULL);
										if (temp) 
										{
											stub.visibility = atof(temp);
											xmlFree(temp); 
										}
									}

									child_L4 = child_L4->next;
								}

								stub_image.push_back(stub);
							}

							child_L3 = child_L3->next;
						}
					}

					detection_result.push_back(stub_image);
				}

				child = child->next;
			}
		}
	}
}

void Detection::toXml(ostream& os) const
{
	//XmlOutputHandler	out(os);

	//xmlTextWriterPtr  writer = xmlNewTextWriter(out.xmlOutputBuffer());

	//xmlTextWriterSetIndent (writer, 1);	
	//xmlTextWriterStartDocument(writer, NULL, "UTF-8", NULL);

	//xmlTextWriterStartElement(writer, XML_TAG_CAMERA);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_NAME,   "%s", mName.c_str());

	//xmlTextWriterStartElement(writer, XML_TAG_GEOMETRY);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_WIDTH, "%d",	mImgWidth);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_HEIGHT,"%d",	mImgHeight);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_NCX,	 "%lf",	mNcx);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_NFX,   "%lf",	mNfx);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_DX,    "%lf",	mDx);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_DY,	 "%lf",	mDy);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_DPX,   "%lf",	mDpx);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_DPY,   "%lf",	mDpy);
	//xmlTextWriterEndElement(writer); // XML_TAG_GEOMETRY


	//xmlTextWriterStartElement(writer, XML_TAG_INTRINSIC);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_FOCAL, "%lf",	mFocal);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_KAPPA1,"%lf",	mKappa1);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_CX,	 "%lf",	mCx);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_CY,    "%lf",	mCy);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_SX,    "%lf",	mSx);
	//xmlTextWriterEndElement(writer); // XML_TAG_INTRINSIC

	//xmlTextWriterStartElement(writer, XML_TAG_EXTRINSIC);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_TX, "%lf",	mTx);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_TY, "%lf",	mTy);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_TZ, "%lf",	mTz);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_RX, "%lf",	mRx);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_RY, "%lf",	mRy);
	//xmlTextWriterWriteFormatAttribute(writer, XML_TAG_RZ, "%lf",	mRz);
	//xmlTextWriterEndElement(writer); // XML_TAG_EXTRINSIC

	//xmlTextWriterEndElement(writer); // XML_TAG_CAMERA

	//xmlTextWriterEndDocument(writer);
	//xmlFreeTextWriter(writer);

}
